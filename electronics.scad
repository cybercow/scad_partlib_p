module perfboard( grid_size = 2.54, thickness = 2, foodprint = 0, offset = 0, n_xy = [3,4],hole_diameter = 1)
{
 difference()
 {
  if( foodprint == 0 )
  {
   if(is_num(offset))
   {
    cube([grid_size * (n_xy[0] - 1) + offset * 2, grid_size * (n_xy[1] -1) + offset * 2, thickness]);
   }else
   {
    cube([grid_size * (n_xy[0] - 1) + offset[0] * 2, grid_size * (n_xy[1] -1) + offset[1] * 2, thickness]);
   }
  }else if( is_num( foodprint ))
  {
   cube([foodprint, foodprint, thickness]);
  }else
  {
   cube([foodprint[0], foodprint[1], thickness]);
  }
  
  for ( i = [0:n_xy[0]-1] )
  {
   for( j = [0:n_xy[1]-1] )
   {
    if( is_num(offset) )
    {
     translate([offset + grid_size * i, offset + grid_size * j, 0]) cylinder( h = 3*thickness, d = hole_diameter, center = true );
    }else
    {
     translate([offset[0] + grid_size * i, offset[1] + grid_size * j, 0]) cylinder( h = 3*thickness, d = hole_diameter, center = true );
    }
   }
  }
 }
}